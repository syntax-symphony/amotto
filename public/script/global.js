export let globalGuessNum = 1;

export function incGuessNum() {
  globalGuessNum += 1;
}

export function resetGlobalGuess() {
  globalGuessNum = 0;
}

export let gameState = 1;

export function changeGameState(num) {
  switch (
    num // input is gameState
  ) {
    case 1: // if gameState == 1
      return "Game Won";
    case 2: // if gameState == 2
      return "Game Over";
    default: // all else, game is suspended
      return "Suspended";
  }
}

export function displayGuess(songData) {
  var x = document.getElementById("song-dropdown");
  var i = x.selectedIndex;
  if (x.options[i].text == songData.songName) {
    console.log(x.options[i].text);
    return true;
  } else {
    return false;
  }
}

// switch variable for prod.
let prodSotdUrl =
  "https://portfolios.talentsprint.com/syntaxsymphony/api/song/currentTrack";
let devSotdUrl = "http://localhost:8107/api/song/currentTrack";
let prodNamesUrl =
  "https://portfolios.talentsprint.com/syntaxsymphony/api/song/names";
let devNamesUrl = "http://localhost:8107/api/song/names";

export async function getSongOfTheDay() {
  try {
    const response = await fetch(devSotdUrl);
    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Error fetching song of the day:", error);
    throw error;
  }
}

export async function getSongNames() {
  try {
    const response = await fetch(devNamesUrl);
    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Error fetching song names:", error);
    throw error;
  }
}

const devGuessUrl = "http://localhost:8107/api/song/guess";
const prodGuessUrl =
  "https://portfolios.talentsprint.com/syntaxsymphony/api/song/guess";

export async function checkGuess(guess) {
  try {
    // Construct the URL for the guess endpoint with the guessed song name
    const queryParams = new URLSearchParams({ songName: guess });
    const url = `${devGuessUrl}?${queryParams}`;

    // Make the API call to the guess endpoint
    const response = await fetch(url);
    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Error checking guess:", error);
    throw error;
  }
}
