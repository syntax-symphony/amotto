import { globalGuessNum, getSongNames, getSongOfTheDay } from "./global.js";

var songs = [];
const songData = await getSongOfTheDay();

function filterSongs() {
  var input = document.getElementById("song-search").value.toLowerCase();
  var dropdown = document.getElementById("song-dropdown");
  dropdown.innerHTML = ""; // Clear previous options

  getSongNames()
    .then((names) => {
      songs = names;

      for (var i = 0; i < songs.length; i++) {
        var song = songs[i].toLowerCase();
        if (song.includes(input)) {
          var option = document.createElement("option");
          option.text = songs[i];
          dropdown.add(option);
        }
      }
    })
    .catch((error) => {
      console.error(error);
    });
}
function displayguess() {
  var x = document.getElementById("song-dropdown").value;
  var i = x.selectedIndex;
  if (x == songData.songName) {
    return true;
  } else {
    return false;
  }
}
// Attach event listener programmatically
document.getElementById("song-search").addEventListener("input", filterSongs);
document.getElementById("guess").addEventListener("click", displayguess);
