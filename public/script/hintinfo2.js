import {
    getSongOfTheDay,
} from "./global.js"

const hintButton = document.getElementById("hint")

var songObj = {    
    "songName": "Bleeding Love",
    "artist": "Leona Lewis",
    "album": "Bleeding Love - Single",
    "releaseYear": "2007",
    "length": 262.293,
    "genre": "Pop",
    "artworkUrl": "https://is1-ssl.mzstatic.com/image/thumb/Features115/v4/59/43/92/594392bc-39a2-bf7d-9401-cd36632423c0/dj.gosywhnl.jpg/100x100bb.jpg",
    "previewUrl": "https://audio-ssl.itunes.apple.com/itunes-assets/AudioPreview116/v4/53/cf/0e/53cf0e7d-3332-7c5a-f534-d55dca3dfc02/mzaf_10515165820178304282.plus.aac.p.m4a",
    "appleId": "266450376",
    "appleMusicUrl": "https://music.apple.com/us/album/bleeding-love/266450354?i=266450376&uo=4",
    "_id": "65c3b1b5da8210558087140f",
    "__v": 0
};

hintButton.addEventListener('click', async function () {
    songObj = await getSongOfTheDay();
    // Access the song object data
    var releaseYear = songObj.releaseYear;
    var length = songObj.length;
    var albumName = songObj.album;
    var artistName = songObj.artist;

    // Update the table cells with the data
    document.getElementById('releaseYearCell').innerText = releaseYear;
    document.getElementById('lengthCell').innerText = length;
    document.getElementById('albumNameCell').innerText = albumName;
    document.getElementById('artistNameCell').innerText = artistName;
});
