import {
  globalGuessNum,
  incGuessNum,
  getSongOfTheDay,
  displayGuess,
  gameState,
  changeGameState,
} from "./global.js";

// Logic Variables ======================================================================

let audioOffset = 10;
const songData = await getSongOfTheDay();
let gameTimeStart = null;
let gameTimeEnd = null;
var playTimes = -1;

// for legacy browsers, Create Audio Buffer

const AudioContext = window.AudioContext || window.webkitAudioContext;
const audioContext = new AudioContext();

const track = audioContext.createBufferSource();
const gainNode = audioContext.createGain();

document.getElementById("guess").innerText = "Guess " + globalGuessNum;

// Document Variabes ====================================================================

const playButton = document.getElementById("play");
const guessButton = document.getElementById("guess");
const volumeSlider = document.getElementById("volumeSlider");
const songSearchContainer = document.querySelector(".song-search-container");
const hintTableContainer = document.getElementById("hintTableContainer");
const originalContent = document.getElementById("originalContent");
const gameOverPage = document.getElementById("gameOverPage");
const winPage = document.getElementById("winPage");
let selectedSong = "";

// Event Listener variables ===============================================
playButton.addEventListener("click", async () => {
  document.getElementById("play").disabled = true;
  const songData = await getSongOfTheDay(); // getSongOfTheDay returns the entire song object
  // console.log(songData)                     // Check to make sure it's the proper song data
  const audioUrl = songData.previewUrl; // PreviewUrl property contains the audio URL
  // console.log(audioUrl)                     // Check to make sure the it's the correct URL
  const response = await fetch(audioUrl);
  const soundBuffer = await response.arrayBuffer();
  const songBuffer = await audioContext.decodeAudioData(soundBuffer);
  const songSource = audioContext.createBufferSource();
  songSource.buffer = songBuffer;
  songSource.connect(gainNode).connect(audioContext.destination);

  let audioDuration = null;
  // Event Listener variables ===============================================

  if (gameState != 1 && gameState != 2) {
    audioDuration = 0;
  } else if (gameState == 1) {
    audioDuration = globalGuessNum * 0.5;
  } else {
    audioDuration = 30;
  }

  songSource.start(0, audioOffset, audioDuration);
  playTimes += 1;
  // Time Tracking
  if (playTimes == 0) {
    changeGameState(1);
    gameTimeStart = new Date();
    console.log(gameTimeStart);
    playTimes += 1;
  }

  if (globalGuessNum == 5) {
    setTimeout(function () {
      document.getElementById("play").disabled = false;
    }, audioDuration * 1000);
  }
  if (globalGuessNum < 5) {
    setTimeout(function () {
      document.getElementById("play").disabled = false;
    }, audioDuration * 1000);
  }
});

guessButton.addEventListener("click", async () => {
  incGuessNum();
  guessButton.innerText = "Guess " + globalGuessNum;
  if (globalGuessNum > 5) {
    document.getElementById("guess").disabled = true;
    console.log("Game Over");
    // Hide the divs when the game is over
    songSearchContainer.style.display = "none";
    hintTableContainer.style.display = "none";
    originalContent.style.display = "none";

    // Show the gameOverPage div
    gameOverPage.classList.remove("hidden");
    return;
  }
  if (displayGuess(songData) === true) {
    console.log("Correct");
    gameTimeEnd = new Date();
    let score1 = Math.round(songData.length * 0.1);
    let timePen = Math.round((gameTimeEnd - gameTimeStart) / 1000);
    let guessPen = Math.round((score1 / 10) * (globalGuessNum - 2));
    let playPen = Math.round((score1 / 1000) * playTimes);
    let totalPen = timePen + guessPen + playPen;
    console.log(timePen, guessPen, playPen);
    console.log(totalPen);
    var fScore = score1 - totalPen;
    console.log(fScore);
    document.getElementById("scorecard").textContent = `Score ${fScore}`;
    // Hide the divs when the game is over
    songSearchContainer.classList.add("hidden");
    hintTableContainer.classList.add("hidden");
    originalContent.classList.add("hidden");
    // Show the winPage div
    winPage.classList.remove("hidden");
  } else {
    selectedSong = document.getElementById("song-dropdown").value;
    console.log("WRONG");
    alert(`Sorry, "${selectedSong}" is not the Song of the Day.`);
    // Hide any displayed hints
    document.getElementById("hintTable").classList.add("hidden");
  }
});

volumeSlider.addEventListener("input", () => {
  gainNode.gain.value = volumeSlider.value;
});
