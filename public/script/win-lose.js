// win-lose.js
import { resetGlobalGuess } from "./global.js";

document.getElementById("aboutButton").addEventListener("click", function () {
  showTooltip("About", event);
});
document.getElementById("statsButton").addEventListener("click", function () {
  showTooltip("Stats", event);
});
document
  .getElementById("howToPlayButton")
  .addEventListener("click", function () {
    showTooltip("How To Play", event);
  });

function showTooltip(content, event) {
  const button = event.target;
  const tooltipContent = `
    <strong>${content}</strong> Tooltip Content
    <button id="closeButton">×</button>`;

  const tooltipContainer = document.getElementById("tooltipContainer");

  const leftPosition = window.innerWidth / 2 - tooltipContainer.offsetWidth / 2;
  const topPosition =
    window.innerHeight / 2 - tooltipContainer.offsetHeight / 2;

  tooltipContainer.innerHTML = tooltipContent;
  tooltipContainer.style.left = `${leftPosition}px`;
  tooltipContainer.style.top = `${topPosition}px`;
  tooltipContainer.style.display = "block";

  const closeButton = document.getElementById("closeButton");
  closeButton.addEventListener("click", hideTooltip);
}

function hideTooltip() {
  const tooltipContainer = document.getElementById("tooltipContainer");
  tooltipContainer.style.display = "none";
}

function triggerWinPage() {
  document.getElementById("winPage").classList.remove("hidden");
  document.getElementById("originalContent").classList.add("hidden");
  console.log("Triggering Win Page...");
}

function triggerWinPageTest() {
  // function for testing purposes
  triggerWinPage();
}

function triggerGameOverPageTest() {
  // function for testing purposes
  triggerGameOverPage();
}

function triggerGameOverPage() {
  document.getElementById("gameOverPage").classList.remove("hidden");
  document.getElementById("originalContent").classList.add("hidden");
  console.log("Triggering Game Over Page...");
}

const playAgainButton = document.getElementById("playAgainButton");

playAgainButton.addEventListener("click", () => {
  restartGame();
});

const returnToMenuButton = document.getElementById("returnToMenuButton");

returnToMenuButton.addEventListener("click", () => {
  returnToMenu();
});

const songSearchContainer = document.querySelector(".song-search-container");
const hintTableContainer = document.getElementById("hintTableContainer");

function restartGame() {
  document.getElementById("winPage").classList.add("hidden");
  document.getElementById("gameOverPage").classList.add("hidden");
  document.getElementById("originalContent").classList.remove("hidden");
  songSearchContainer.classList.remove("hidden");
  hintTableContainer.classList.remove("hidden");
  resetGlobalGuess();
  console.log("Game restarted!");
}

function returnToMenu() {
  document.getElementById("winPage").classList.add("hidden");
  document.getElementById("gameOverPage").classList.add("hidden");
  document.getElementById("originalContent").classList.remove("hidden");
  songSearchContainer.classList.remove("hidden");
  hintTableContainer.classList.remove("hidden");
  resetGlobalGuess();
  console.log("Returned to menu.");
}
