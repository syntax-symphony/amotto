import {
    globalGuessNum, getSongOfTheDay
} from "./global.js"
const songData = await getSongOfTheDay();

// let currentHintIndex = 3;
const hints = document.querySelectorAll("#hintTable tbody tr");
const hintButton = document.getElementById("hint")

function toggleHints() {
    console.log(globalGuessNum)
    if (globalGuessNum > 1) { // Check if a guess has been made
        const rowCount = document.querySelectorAll('#hintTable tbody tr').length;
        console.log('Number of rows:', rowCount);
        const hintsTable = document.getElementById("hintTable");
        if (hintsTable.style.display == "none") {
            hintsTable.style.display = "block";
            showNextHint(); // Show the first row immediately when the table is displayed
        } else {
            hintsTable.style.display = "none";
        }
    }
}

function showNextHint() {
    // Hide all rows
    hints.forEach(function (row) {
        row.style.display = "none";
    });

     // Display the hint rows for the appropriate guesses
    if (globalGuessNum >= 2 && globalGuessNum <= 5) {
        hints[globalGuessNum - 2].style.display = "table-row";
    }
    // // Update the index for the next row
    // currentHintIndex = (currentHintIndex + 1) % hints.length;

    // If all rows have been shown, reset the index to 0
    if (globalGuessNum === 0) {
        document.getElementById("hintTable").style.display = "none"; // Hide the table after showing all rows
    }
}

hintButton.addEventListener('click', async () =>{
    toggleHints()
    console.log("click") 
})

