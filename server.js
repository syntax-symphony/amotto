require("dotenv").config();
const express = require('express');
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const app = express();
const router = express.Router();
const port = process.env.PORT;
const viewRoutes = require("./routes/viewRoutes");
const apiRoutes = require("./routes/apiRoutes");

const loggerMiddleware = (req, res, next) => {
  console.log(req.url, req.method, res.statusCode);
  next();
};

app.use(
  bodyParser.urlencoded({ extended: true }),
  bodyParser.json(),
  loggerMiddleware
);

router.use(viewRoutes);
router.use("/api", apiRoutes);

const baseUrl = process.env.MODE == "production" ? "" : "/"
app.use(baseUrl, express.static("public"));
app.use(baseUrl, router);

let url = "mongodb://localhost:27017/";
let db = "songs";
if(process.env.MODE == "production"){
  url = `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@192.168.171.67/syntax_symphony?authSource=admin`
  db = "syntax_symphony"
}

async function main() {
  await mongoose.connect(url, { dbName: db });
  app.listen(port, () => {
    console.log(`app is listening on port ${port}`);
  });
}

main().catch((err) => console.error(err)).then(() => console.log("main success"));
