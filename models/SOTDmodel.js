const mongoose = require("mongoose");

const sotdSchema = new mongoose.Schema({
  songName: {type: String, required: [true, 'Song Name is required.']},
  artist: {type: String, required: [true, 'Artist is required.']},
  album: {type: String, required: [true, 'Album is required.']},
  releaseYear: {type: String, required: [true, 'Release Year is required.']},
  length: {type: Number, required: [true, 'Length is required']},
  genre: {type: String, required: [true, 'Genre is required']},
  artworkUrl: {type: String, required: [true, 'Artwork URL is required']},
  previewUrl: {type: String, required: [true, 'Preview URL is required']},
  appleId: {type: String, required: [true, 'Apple ID is required'], unique: true},
  appleMusicUrl: {type: String},
  date: {type: Date, required: [true, 'Date is required']}
})

const SOTD = mongoose.model("SOTD", sotdSchema, 'sotd');
module.exports = SOTD;