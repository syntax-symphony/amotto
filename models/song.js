const mongoose = require("mongoose");

const songSchema = new mongoose.Schema({
  songName: {type: String, required: [true, 'Song Name is required.']},
  artist: {type: String, required: [true, 'Artist is required.']},
  album: {type: String, required: [true, 'Album is required.']},
  releaseYear: {type: String, required: [true, 'Release Year is required.']},
  length: {type: Number, required: [true, 'Length is required']},
  genre: {type: String, required: [true, 'Genre is required']},
  artworkUrl: {type: String, required: [true, 'Artwork URL is required']},
  previewUrl: {type: String, required: [true, 'Preview URL is required']},
  appleId: {type: String, required: [true, 'Apple ID is required'], unique: true},
  appleMusicUrl: {type: String},
  date: {type: Date}
})

const song = mongoose.model("song", songSchema, 'song_data');
module.exports = song;