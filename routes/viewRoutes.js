const path = require("path") 
const express = require('express');
const router = express.Router();

const serveLandingPage = (req, res) => {
  res.sendFile(path.join(__dirname, "../public/index.html"));
}

router.get('/', serveLandingPage);
router.get('/home', serveLandingPage);

module.exports = router;