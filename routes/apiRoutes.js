const {Router} = require("express");
const songRouter = Router();
const apiRouter = Router();
const axios = require('axios');
const mongoClient = require("../db/connection");
// check for production mode in order to connect to the correct db
const db = mongoClient.db(
  process.env.MODE == 'production' ? "syntax_symphony" : "songs"
  );
const song = require("../models/song");
const SOTD = require('../models/SOTDmodel');
const { MongoError } = require('mongodb');
const { validationResult } = require('express-validator');
  
// Endpoint to get all songs in the db
songRouter.get("/all", async (req, res) => {
  try {
    // Fetch all songs using Mongoose model
    const allSongs = await song.find({});

    // Send the response with the retrieved songs
    res.json(allSongs);
  } catch (err) {
    console.error(err);
    res.status(500).json("Error fetching songs");
  }
});

// Endpoint to get the names of all songs in the db
songRouter.get("/names", async (req, res) => {
  try {
    // Fetch all songs where "songName" property exists
    const filteredSongs = await song.find({ songName: { $exists: true } }, 'songName');

    // Extract only the "songName" properties from the filtered documents
    const songNames = filteredSongs.map(song => song.songName);

    // Send the response with the retrieved song names
    res.json(songNames);
  } catch (err) {
    console.error(err);
    res.status(500).json("Error fetching song names");
  }
});

// Endpoint to set or retrieve the current SOTD
songRouter.get("/currentTrack", async (req, res) => {
  try {
    const todayUTC = new Date(Date.UTC(new Date().getUTCFullYear(), new Date().getUTCMonth(), new Date().getUTCDate()));

    // Check for a current mystery song
    let sotd = await SOTD.findOne({ date: todayUTC }); 

    // If there is already a SOTD with today's date, return that song object
    if (sotd) {
      res.json(sotd);
    } else { 
      // If no current SOTD, then we want to get a new song that hasn't already been used
      const usedAppleIds = await SOTD.distinct("appleId");
      
      // Exclude AppleIds of previous mystery songs and return a single random song object
      const pipeline = [
        { $match: { appleId: { $nin: usedAppleIds } } }, 
        { $sample: { size: 1 } }
      ];

      // Query the database to find a new song
      const newSong = await song.aggregate(pipeline).exec();

      if (!newSong || newSong.length === 0) {
        // If no new songs are available, return an error
        return res.status(404).json({ error: 'No songs available' });
      }

      // Create a new SOTD document
      const sotdData = {
        ...newSong[0],
        date: todayUTC
      };
      
      // Add the new SOTD to the database to record it and return the new SOTD
      const createdSOTD = await SOTD.create(sotdData);
      res.json(createdSOTD);
    }
  } catch (error) {
    console.error('Error getting current track:', error);
    res.sendStatus(404);
  }
});

// Endpoint to get the appleid by a song name match
songRouter.get("/appleId", async (req, res) => {
  try {
    const { songName } = req.query;

    // Check if songName query parameter is provided
    if (!songName) {
      return res.status(400).json({ error: 'Song name query parameter is required' });
    }

    // Search for the song in the database by its name
    const foundSong = await song.findOne({ songName });

    // Check if the song exists in the database
    if (!foundSong) {
      return res.status(404).json({ error: 'Song not found' });
    }

    // Return the Apple ID of the found song
    res.json({ appleId: foundSong.appleId });
  } catch (error) {
    console.error('Error getting Apple ID:', error);
    res.sendStatus(500);
  }
});

// Endpoint to check if a guess is correct
songRouter.get("/guess", async (req, res) => {
  try {
    const { songName } = req.query;

    // Log the received songName query parameter
    console.log('Received songName:', songName);

    // Check if songName query parameter is provided
    if (!songName) {
      console.log('No songName provided.');
      return res.status(400).json({ error: 'Song name query parameter is required' });
    }

    const todayUTC = new Date(Date.UTC(new Date().getUTCFullYear(), new Date().getUTCMonth(), new Date().getUTCDate()));

    // Log the date being used for querying
    // console.log('Query date:', todayUTC);

    // Check for the current SOTD
    const currentSOTD = await SOTD.findOne({ date: todayUTC });

    // Log the result of the database query
    // console.log('Current SOTD:', currentSOTD);

    // If there is no current SOTD, return a 404 Not Found response
    if (!currentSOTD) {
      console.log('No current Song of the Day available.');
      return res.status(404).json({ error: 'No current Song of the Day available' });
    }

    // Check if the provided song name matches the current SOTD
    const match = currentSOTD.songName.toLowerCase() === songName.toLowerCase();

    // Log the comparison result
    console.log('Song name match:', match);

    // Return the guess result along with the name of the current SOTD
    res.json({ guess: match, currentSOTD: currentSOTD.songName });
    } catch (error) {
      console.error('Error checking guess:', error);
      res.sendStatus(500);
    }
});
// Endpoint to create a new song from Apple Music API
songRouter.post('/:appleId', async (req, res) => {
  try {
    // Validate request parameters
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { appleId } = req.params;

    // Make request to Apple Music API to get song details
    const response = await axios.get(`https://itunes.apple.com/lookup?id=${appleId}`);

    // Check if the request to Apple Music API was successful
    if (response.status !== 200) {
      console.error("Error fetching song details:", response.data);
      return res.sendStatus(500);
    }

    // Extract relevant song information
    const responseData = response.data;
    const data = responseData.results[0];

    if (!data) {
      return res.status(404).json({ error: 'Song not found' });
    }
    
    const songInfo = {
      songName: data.trackName,
      artist: data.artistName,
      album: data.collectionName,
      releaseYear: data.releaseDate.substring(0, 4),
      length: data.trackTimeMillis / 1000, // convert to seconds
      genre: data.primaryGenreName,
      artworkUrl: data.artworkUrl100,
      previewUrl: data.previewUrl,
      appleId,
      appleMusicUrl: data.trackViewUrl
    };

    // Save the new song to the database
    const newSong = new song(songInfo);
    await newSong.save();

    res.status(201).json({ message: 'Song created successfully', song: newSong });
  } catch (error) {
    console.error('Error creating song:', error);

    // Check if the error is a duplicate key error for the 'appleId' field
    if (error instanceof MongoError && error.code === 11000 && error.keyPattern && error.keyPattern.appleId) {
      // If it's a duplicate key error for the 'appleId' field, send a custom error message
      return res.status(400).json({ error: 'Song with the provided Apple ID already exists.' });
    }
    
    // Otherwise, send a generic error message
    res.status(500).json({ error: 'Internal server error' });
  }
});

apiRouter.use("/song", songRouter)
module.exports = apiRouter;
