import requests
import re
running = True
songDictList = []
while running != False:
    inpt = input("Search for a song?: ")
    if inpt == 'n':
        running = False
        break
    inpt = re.sub("\s", "+", inpt)
    res = requests.get(f'https://itunes.apple.com/search?term={inpt}')
    searchResult = res.json()
    songObj = searchResult['results'][1]
    print(songObj)
    ans = input("Select this song: y/n \n")
    if ans == "y":
        tempObj= {    
        "songName": songObj["trackName"],
        "artist": songObj["artistName"],
        "album": songObj["collectionCensoredName"],
        "releaseYear": songObj["releaseDate"][0:4],
        "length": songObj["trackTimeMillis"],
        "genre": songObj["primaryGenreName"],
        "artworkUrl": songObj["artworkUrl100"],
        "previewUrl": songObj["previewUrl"],
        "appleId": songObj["trackId"],
        "appleMusicUrl": songObj["trackViewUrl"],
        "__v": ""}
        songDictList.append(tempObj)
    else:
        running = False
else:
    print(songDictList)
