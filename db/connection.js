const {MongoClient} = require("mongodb");
let url = "mongodb://localhost:27017/";
if(process.env.MODE == "production"){
  url = `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@192.168.171.67/?authSource=admin`
}
const mongoClient = new MongoClient(url)
module.exports = mongoClient;